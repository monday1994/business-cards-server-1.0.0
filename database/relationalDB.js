const Promise = require('bluebird');
const Sequelize = require('sequelize');

console.log("db name = "+process.env.DB_NAME);
console.log("db username = "+ process.env.DB_USERNAME);
console.log("db pass = "+ process.env.DB_PASS);
console.log("host = " + process.env.DB_HOST);

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASS, {
    dialect : process.env.DB_DIALECT,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT
}, {operatorAliases : false});


// Connect all the models/tables in the database to a db object,
//so everything is accessible via one object
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

const initEntities = function(){

    //Models/tables
    db.Admin = require('./models/Admin')(sequelize, Sequelize);
    db.BusinessCard = require('./models/BusinessCard')(sequelize, Sequelize);
    db.CompanyDetails = require('./models/CompanyDetails')(sequelize, Sequelize);
    db.Notification = require('./models/Notification')(sequelize, Sequelize);
    db.Pattern = require('./models/Pattern')(sequelize, Sequelize);
    db.Room = require('./models/Room')(sequelize, Sequelize);
    db.User = require('./models/User')(sequelize, Sequelize);

    return new Promise((resolve, reject) => {
        //creating relations
        Promise.all([

            // 1-M User - BusinessCard
            db.User.hasMany(db.BusinessCard, { as : 'BusinessCards'}),
            db.BusinessCard.belongsTo(db.User, { as : 'User'}),

            // 1-M User - CompanyDetails
            db.User.hasMany(db.CompanyDetails, { as : 'CompaniesDetails'}),
            db.CompanyDetails.belongsTo(db.User, { as : 'User' }),

            // 1-1 BusinessCard - CompanyDetails
            db.BusinessCard.belongsTo(db.CompanyDetails, { as : 'CompanyDetails'}),

            // M:N BusinessCard - BusinessCard
            db.BusinessCard.belongsToMany(db.BusinessCard, {as : 'Contacts', through : {model : 'CardCard', unique : false}, foreignKey : 'BusinessCardId'}),

            // 1-M BoughtPattern - BusinessCard
            db.Pattern.hasMany(db.BusinessCard, { as : 'BusinessCards'}),
            db.BusinessCard.belongsTo(db.Pattern, { as : 'Pattern'}),

            // M-N BoughtPattern - User
            db.User.belongsToMany(db.Pattern, {as : 'Patterns', through : {model : 'UserPattern', unique : false}, foreignKey : 'UserId'}),
            db.Pattern.belongsToMany(db.User, {as : 'Users', through : {model : 'UserPattern', unique : false}, foreignKey : 'PatternId'}),

        ]).then((results)=>{
            console.log("db has been initialized");
            resolve(results);
        }).catch(err => {
            console.error('db error = '+err);
            reject(err);
        });
    });
};

db.initDb = function(isForced){
    return new Promise((resolve, reject) => {

        initEntities().then((results)=>{
            db.sequelize.sync({force : isForced}).then(() => {
                console.log("db initialized");
                resolve('database initialized');
            }).catch(err => {
                console.error(err);
                reject(err);
            });
        }).catch(err=>{
            console.log("error occured when entities were initializing");
            reject(err);
        });


    });
};


module.exports = {
    db : db,
    sequelize : sequelize,
    Sequelize : Sequelize
};
