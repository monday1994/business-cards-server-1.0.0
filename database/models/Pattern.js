const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const Pattern = sequelize.define("Pattern", {
        name : {
            type : DataTypes.STRING
        },
        linkToObvers : {
            type : DataTypes.STRING
        },
        linkToRevers : {
            type : DataTypes.STRING
        },
        price : {
            type : DataTypes.FLOAT
        }
    }, {
        timestamps: false
    });

    return Pattern;
};