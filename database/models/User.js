const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("User", {
        name : {
            type : DataTypes.STRING
        },
        lastname : {
            type : DataTypes.STRING
        },
        email: {
            type : DataTypes.STRING,
            unique : true
        },
        password : {
            type : DataTypes.STRING
        },
        forgotPasswordToken : {
            type : DataTypes.STRING
        },
        forgotPasswordTokenExpiry : {
            type : DataTypes.BIGINT
        },
        linkToProfilePhoto : {
            type : DataTypes.STRING
        }
    }, {
        timestamps: false
    });

    return User;
};