const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const BusinessCard = sequelize.define("BusinessCard", {
        colors : {
            type : DataTypes.JSON
        }
    }, {
        timestamps: false
    });

    return BusinessCard;
};