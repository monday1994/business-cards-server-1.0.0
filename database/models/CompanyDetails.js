const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const CompanyDetails = sequelize.define("CompanyDetails", {
        name : {
            type : DataTypes.STRING
        },
        role : {
            type : DataTypes.STRING
        },
        website: {
            type : DataTypes.STRING
        },
        phone : {
            type : DataTypes.STRING
        },
        address : {
            type : DataTypes.STRING
        },
        NIP : {
            type : DataTypes.STRING
        },
        REGON : {
            type : DataTypes.STRING
        },
        KRS : {
            type : DataTypes.STRING
        },
        description : {
            type : DataTypes.STRING
        }
    }, {
        timestamps: false
    });

    return CompanyDetails;
};