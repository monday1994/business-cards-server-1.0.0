const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const Admin = sequelize.define("Admin", {
        name : {
            type : DataTypes.STRING
        },
        lastname : {
            type : DataTypes.STRING
        },
        email: {
            type : DataTypes.STRING,
            unique : true
        },
        password : {
            type : DataTypes.STRING
        },
        phone : {
            type : DataTypes.STRING
        },
        location : {
            type : DataTypes.STRING
        },
        forgotPasswordToken : {
            type : DataTypes.STRING
        },
        forgotPasswordTokenExpiry : {
            type : DataTypes.BIGINT
        }
    }, {
        timestamps: false
    });

    return Admin;
};