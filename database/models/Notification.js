const VP = require('../../configuration/validationParameters');

module.exports = (sequelize, DataTypes) => {
    const Notification = sequelize.define("Notification", {
        UserId : {
            type : DataTypes.INTEGER
        },
        type : {
            type : DataTypes.STRING
        },
        message : {
            type : DataTypes.STRING
        },
        isNew : {
            type : DataTypes.BOOLEAN
        },
        isReadOut : {
            type : DataTypes.BOOLEAN
        },
        associatedData : {
            type : DataTypes.JSON
        }
    }, {
        timestamps: true
    });
    return Notification;
};