const util = require('util');
const Room = require('../relationalDB').db.Room;


exports.create = function(newRoom){
    return new Promise((resolve, reject) => {
        Room.create(newRoom).then(createdRoom => {
            resolve(createdRoom);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getById = function(id){
    return new Promise((resolve, reject)=>{
        Room.findOne({where : {id : id}}).then(roomFromDb => {
            if(roomFromDb){
                resolve(roomFromDb);
            } else {
                reject('Room with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.update = function(roomToBeUpdate, creatorId){
    return new Promise((resolve, reject)=>{
        Room.findOne({where : {id : roomToBeUpdate.id}}).then(roomFromDb => {
            if(roomFromDb){
                if(roomFromDb.creator === creatorId){
                    roomFromDb.update(roomToBeUpdate).then(updatedRoom => {
                        resolve(updatedRoom);
                    }).catch(err =>{
                        reject(err);
                    });
                } else {
                    reject('Only room creator can update room');
                }
            } else {
                reject('room with id : '+roomToBeUpdate.id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = function(roomId){
    return new Promise((resolve, reject) => {
        Room.destroy({where : {id : roomId}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        })
    });
};
