const util = require('util');
const db = require('../relationalDB').db;
const BusinessCard = db.BusinessCard;
const async = require('async');

exports.create = function(newBusinessCard){
    return new Promise((resolve, reject) => {
        BusinessCard.create(newBusinessCard).then(createdBusinessCard => {
            resolve(createdBusinessCard);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getByIdWithAssociatedPatternAndCompanyDetails = function(id){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}, include : [
                {
                    model : db.CompanyDetails, as : 'CompanyDetails'
                },
                {
                    model : db.Pattern, as : 'Pattern'
                }
            ]}).then(businessCardFromDb => {
            if(businessCardFromDb && businessCardFromDb.Pattern){
                resolve(businessCardFromDb);
            } else {
                reject('BusinessCard with id : '+id+' does not exist in db or does not have associated pattern');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getByIdWithAssociatedData = function(id){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}, include : [
                {
                    model : db.CompanyDetails, as : 'CompanyDetails'
                },
                {
                    model : db.Pattern, as : 'Pattern'
                },
                {
                    model : db.BusinessCard, as : 'Contacts'
                }
            ]}).then(businessCardFromDb => {
            if(businessCardFromDb){
                resolve(businessCardFromDb);
            } else {
                reject('BusinessCard with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getById = function (id) {
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}}).then(businessCardFromDb => {
            if(businessCardFromDb){
                resolve(businessCardFromDb);
            } else {
                reject('BusinessCard with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getCardWithContactsById = function(id){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : id}, include : [
                {
                    model : db.BusinessCard, as : 'Contacts', attributes : ['UserId'], through : {attributes : ['ContactId']}
                }
            ]}).then(businessCardFromDb => {
            if(businessCardFromDb){
                resolve(businessCardFromDb)
            } else {
                reject('BusinessCard with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.update = function(businessCardToBeUpdate){
    return new Promise((resolve, reject)=>{
        BusinessCard.findOne({where : {id : businessCardToBeUpdate.id}}).then(businessCardFromDb => {
            if(businessCardFromDb){
                businessCardFromDb.update(businessCardToBeUpdate).then(updatedBusinessCard => {
                    resolve(updatedBusinessCard);
                }).catch(err =>{
                    reject(err);
                })
            } else {
                reject('business card with id :'+businessCardToBeUpdate.id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = function(businessCardId){
    return new Promise((resolve, reject) => {
        BusinessCard.destroy({where : {id : businessCardId}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        })
    });
};
