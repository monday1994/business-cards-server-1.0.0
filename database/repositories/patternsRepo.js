const util = require('util');
const db = require('../relationalDB').db;
const Pattern = db.Pattern;


exports.create = function(newPattern){
    return new Promise((resolve, reject) => {
        Pattern.create(newPattern).then(createdPattern => {
            resolve(createdPattern);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getAll = function(){
    return new Promise((resolve, reject)=>{
        Pattern.findAll().then(patternsFromDb => {
            resolve(patternsFromDb);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getById = function(id){
    return new Promise((resolve, reject)=>{
        Pattern.findOne({where : {id : id}}).then(patternFromDb => {
            if(patternFromDb){
                resolve(patternFromDb);
            } else {
                reject('Pattern with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getByIdWithAssociatedUsers = function(id){
    return new Promise((resolve, reject)=>{
        Pattern.findOne({where : {id : id}, include : [
            {
                model : db.User, as : 'Users'
            }
            ]
        }).then(patternFromDb => {
            if(patternFromDb){
                resolve(patternFromDb);
            } else {
                reject('Pattern with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.update = function(patternToBeUpdate){
    return new Promise((resolve, reject)=>{
        Pattern.findOne({where : {id : patternToBeUpdate.id}}).then(patternFromDb => {
            if(patternFromDb){
                patternFromDb.update(patternToBeUpdate).then(updatedPattern => {
                    resolve(updatedPattern);
                }).catch(err =>{
                    reject(err);
                })
            } else {
                reject('pattern with id :'+patternToBeUpdate.id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = function(patternId){
    return new Promise((resolve, reject) => {
        Pattern.destroy({where : {id : patternId}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        })
    });
};
