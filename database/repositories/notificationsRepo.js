const util = require('util');
const Notification = require('../relationalDB').db.Notification;


exports.create = function(newNotification){
    return new Promise((resolve, reject) => {
        Notification.create(newNotification).then(createdNotification => {
            resolve(createdNotification);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getAllNewNotifs = function(userId){
    return new Promise((resolve, reject)=>{
        Notification.findAll({where : {UserId : userId, isNew : true}}).then(notifications => {
            resolve(notifications);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getAllNotReadOutNotifs = function(userId){
    return new Promise((resolve, reject)=>{
        Notification.findAll({where : {UserId : userId, isReadOut : false}}).then(notifications => {
            resolve(notifications);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getById = function(id){
    return new Promise((resolve, reject)=>{
        Notification.findOne({where : {id : id}}).then(notification => {
            if(notification){
                resolve(notification);
            } else {
                reject('Notification with id : '+id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getByUserId = function(userId){
    return new Promise((resolve, reject)=>{
        Notification.findAll({where : {UserId : userId}}).then(notifications => {
            if(notifications){
                resolve(notifications);
            } else {
                reject('Notifications with UserId : '+userId+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.update = function(notificationToBeUpdate, ownerId){
    return new Promise((resolve, reject)=>{
        Notification.findOne({where : {id : notificationToBeUpdate.id}}).then(notificationFromDb => {
            if(notificationFromDb){
                if(notificationFromDb.UserId === ownerId){
                    notificationFromDb.update(notificationToBeUpdate).then(updatedNotification => {
                        resolve(updatedNotification);
                    }).catch(err =>{
                        reject(err);
                    });
                } else {
                    reject('Only notification owner can update notification');
                }
            } else {
                reject('Notification with id : '+notificationToBeUpdate.id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = function(id){
    return new Promise((resolve, reject) => {
        Notification.destroy({where : {id : id}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        })
    });
};
