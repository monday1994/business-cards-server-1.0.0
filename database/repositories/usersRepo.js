const util = require('util');
const db = require('../relationalDB').db;
const User = db.User;

exports.createUsingTransaction = function(newUser, t){
    return User.create(newUser, {transaction: t});
};

exports.create = function(newUser){
    return new Promise((resolve, reject) => {
        User.create(newUser).then(createdUser => {
           resolve(createdUser);
        }).catch(err => {
            reject(err);
        });
    });
};




exports.updateUsingTransaction = function(userToBeUpdate, t){

};

exports.update = function(userToBeUpdate){
    return new Promise((resolve, reject)=>{
        User.findOne({where : {id : userToBeUpdate.id}}).then(userFromDb => {
            if(userFromDb){
                userFromDb.update(userToBeUpdate).then(updatedUser => {
                    resolve(updatedUser);
                }).catch(err =>{
                    reject(err);
                })
            } else {
                reject('user with id : '+userToBeUpdate.id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeByIdUsingTransaction = function(userId, t){
    return User.destroy({where : {id : userId}}, {transaction : t});
};

exports.removeById = function(userId){
    return new Promise((resolve, reject) => {
       User.destroy({where : {id : userId}}).then(result => {
           resolve(result);
       }).catch(err => {
           reject(err);
       })
    });
};

exports.getAllIds = function(){
    return new Promise((resolve,reject)=>{
        User.findAll({attributes : ['id']}).then((users)=>{
            resolve(users);
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getByEmail = function(email){
    return new Promise((resolve,reject)=>{
        User.findOne({where: {email : email}}).then((user)=>{
            if(user){
                resolve(user);
            } else {
                reject('user with email : '+email+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getById = function(id){
    return new Promise((resolve,reject)=>{
        User.findOne({where: {id : id}}).then((user)=>{
            if(user){
                resolve(user);
            } else {
                reject('user with id : '+id+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getUserPatterns = function(userId){
    return new Promise((resolve,reject)=>{
        User.findOne({where: {id : userId}, include : [
                {
                    model : db.Pattern, as : 'Patterns'
                }
            ], attributes : ['id', 'name', 'lastname']}).then((user)=>{
            if(user){
                resolve(user.Patterns);
            } else {
                reject('user with id : '+id+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getUserContacts = function(userId){
    return new Promise((resolve,reject)=>{
        User.findOne({where: {id : userId}, include : [
                {
                    model : db.BusinessCard, as : 'BusinessCards', include : [
                        {
                            model : db.BusinessCard, as : 'Contacts', attributes : ['UserId'], through : {attributes : []}
                        }
                    ]
                }
            ], attributes : ['id', 'name', 'lastname', 'email']}).then((user)=>{
            if(user){

                let dataToBeReturn = [];

                if(user.BusinessCards.length){
                    user.BusinessCards.forEach((currentCard, i)=>{
                        dataToBeReturn = dataToBeReturn.concat(currentCard.Contacts);
                        if(user.BusinessCards.length - 1 === i){
                            resolve(dataToBeReturn);
                        }
                    });
                } else {
                    resolve(false);
                }
            } else {
                reject('user with id : '+userId+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.verifyEmailAndId = function(email, id){
    return new Promise((resolve,reject)=>{
        User.findOne({where: {email : email, id : id}}).then((user)=>{
            if(user){
                resolve(user);
            } else {
                reject('user does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.validateLogin = function(email, password){
    return new Promise((resolve, reject) => {
        User.findOne({where: {email : email, password : password}}).then((user)=>{
            if(user){
                resolve(user);
            } else {
                reject('user does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.validateOldPassword = function(email, oldPassword){
    return new Promise((resolve, reject) => {
        User.findOne({where: {email : email, password : oldPassword}}).then((user)=>{
            if(user){
                resolve(user);
            } else {
                reject('wrong email or password');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getByForgotPasswordToken = function(token){
    return new Promise((resolve, reject) => {
        User.findOne({where: {forgotPasswordToken : token}}).then((user)=>{
            if(user){
                let now = new Date().getTime();

                if(user.forgotPasswordTokenExpiry < now){
                    reject('reset token expired');
                } else {
                    resolve(user);
                }
            } else {
                reject('user with token : '+token+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};
