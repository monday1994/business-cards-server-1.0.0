const util = require('util');
const CompanyDetails = require('../relationalDB').db.CompanyDetails;


exports.create = function(newCompanyDetails){
    return new Promise((resolve, reject) => {
        CompanyDetails.create(newCompanyDetails).then(createdCompanyDetails => {
            resolve(createdCompanyDetails);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.getById = function(id){
    return new Promise((resolve, reject)=>{
        CompanyDetails.findOne({where : {id : id}}).then(companyDetailsFromDb => {
            if(companyDetailsFromDb){
                resolve(companyDetailsFromDb);
            } else {
                reject('Company details with id : '+id+' do not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.update = function(companyDetailsToBeUpdate, ownerId){
    return new Promise((resolve, reject)=>{
        CompanyDetails.findOne({where : {id : companyDetailsToBeUpdate.id}}).then(companyDetailsFromDb => {
            if(companyDetailsFromDb){
               if(companyDetailsFromDb.UserId === ownerId){
                   companyDetailsFromDb.update(companyDetailsToBeUpdate).then(updatedCompanyDetails => {
                       resolve(updatedCompanyDetails);
                   }).catch(err =>{
                       reject(err);
                   });
               } else {
                   reject('user with id : '+ownerId+' is not company details creator therefore cannot perform this operation');
               }
            } else {
                reject('company details with id : '+companyDetailsToBeUpdate.id+' do not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = function(companyDetailsId, ownerId){
    return new Promise((resolve, reject) => {
        CompanyDetails.findOne({where : {id : companyDetailsId}}).then(companyDetailsFromDb => {
            if(companyDetailsFromDb){
                if(companyDetailsFromDb.UserId === ownerId){
                    CompanyDetails.destroy({where : {id : companyDetailsId}}).then(result => {
                        resolve(result);
                    }).catch(err => {
                        reject(err);
                    })
                } else {
                    reject('user with id : '+ownerId+' is not company details creator therefore cannot perform this operation');
                }
            } else {
                reject('company details with id : '+companyDetailsId+' do not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};
