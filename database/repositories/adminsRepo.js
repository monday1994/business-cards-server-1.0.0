const util = require('util');
const Admin = require('../relationalDB').db.Admin;

exports.create = function(newAdmin){
    return new Promise((resolve, reject) => {
        Admin.create(newAdmin).then(createdAdmin => {
            resolve(createdAdmin);
        }).catch(err => {
            reject(err);
        });
    });
};

exports.update = function(adminToBeUpdate){
    return new Promise((resolve, reject)=>{
        Admin.findOne({where : {id : adminToBeUpdate.id}}).then(adminFromDb => {
            if(adminFromDb){
                adminFromDb.update(adminToBeUpdate).then(updatedUser => {
                    resolve(updatedUser);
                }).catch(err =>{
                    reject(err);
                })
            } else {
                reject('admin with id : '+adminToBeUpdate.id+' does not exist in db');
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeById = function(adminId){
    return new Promise((resolve, reject) => {
        Admin.destroy({where : {id : adminId}}).then(result => {
            resolve(result);
        }).catch(err => {
            reject(err);
        })
    });
};



exports.verifyEmailAndId = function(email, id){
    return new Promise((resolve,reject)=>{
        Admin.findOne({where: {email : email, id : id}}).then((admin)=>{
            if(admin){
                resolve(admin);
            } else {
                reject('admin does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.validateLogin = function(email, password){
    return new Promise((resolve, reject) => {
        Admin.findOne({where: {email : email, password : password}}).then((admin)=>{
            if(admin){
                resolve(admin);
            } else {
                reject('admin does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.validateOldPassword = function(email, oldPassword){
    return new Promise((resolve, reject) => {
        Admin.findOne({where: {email : email, password : oldPassword}}).then((user)=>{
            if(user){
                resolve(user);
            } else {
                reject('wrong email or password');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getByEmail = function(email){
    return new Promise((resolve,reject)=>{
        Admin.findOne({where: {email : email}}).then((admin)=>{
            if(admin){
                resolve(admin);
            } else {
                reject('admin with email : '+email+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};

exports.getByForgotPasswordToken = function(token){
    return new Promise((resolve, reject) => {
        Admin.findOne({where: {forgotPasswordToken : token}}).then((admin)=>{
            if(admin){
                let now = new Date().getTime();
                if(admin.forgotPasswordTokenExpiry < now){
                    reject('reset token expired');
                } else {
                    resolve(admin);
                }
            } else {
                reject('admin with token : '+token+' does not exist in db');
            }
        }).catch((err)=>{
            reject(err);
        });
    });
};