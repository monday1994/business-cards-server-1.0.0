const _ = require('lodash');

exports.removeObjectByKey = function(arr, key){
    const foundObject = _.find(arr, key);
    return _.pull(arr, foundObject);
};

const splitArray = function(arr, n) {
    let rest = arr.length % n,
        restUsed = rest,
        partLength = Math.floor(arr.length / n),
        result = [];

    for(let i = 0; i < arr.length; i += partLength) {
        let end = partLength + i,
            add = false;

        if(rest !== 0 && restUsed) {
            end++;
            restUsed--;
            add = true;
        }

        result.push(arr.slice(i, end));

        if(add) {
            i++;
        }
    }

    return result;
};

exports.splitArray = splitArray;
