const imageMagick = require('imagemagick');
const async = require('async');
const Promise = require('bluebird');

exports.imageMagickResize = function(buffer, xDimension, yDimension){
    return new Promise((resolve,reject) => {
        async.waterfall([
            function(callback){
                imageMagick.resize({
                    srcData : buffer,
                    width : xDimension,
                    height : yDimension,
                    format: 'jpg',
                    quality : 1,
                    sharpening : 0.2
                }, function(err, stdout, stderr){
                    if(err) return callback(err);
                    callback(null, stdout);
                });
            },
            function(resizedImage, callback){
                imageMagick.crop({
                    srcData : resizedImage,
                    format: 'jpg',
                    width : xDimension,
                    height : yDimension,
                    quality : 1,
                    sharpening : 0.7,
                    gravity: "Center"
                }, function(err, stdout, stderr){
                    if(err) return callback(err);
                    callback(null, stdout);
                });
            }
        ], function(err, fullyResizedImage){
            if(err){
                reject(err);
            } else {
                let convertedToBuffer = new Buffer(fullyResizedImage, 'binary');
                resolve(convertedToBuffer);
            }
        });
    });
};