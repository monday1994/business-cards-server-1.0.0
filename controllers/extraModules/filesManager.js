const fs = require('fs');
const paths = require('../../configuration/paths');
const rmdir = require('rmdir');
const Promise = require('bluebird');

function checkFileExists(filepath){
    return new Promise((resolve, reject) => {
        fs.access(filepath, fs.F_OK, error => {
            if(error){
                reject('file does not exist');
            } else {
                resolve(true);
            }
        });
    });
}

exports.doesFileExists = checkFileExists;

const removeFileByLink = function(link){
    const pathToFilesDirectory = paths.pathToUsersImagesDirectory;
    return new Promise((resolve,reject) => {
        let nameAndId = link.split('getProfilePhoto')[1];
        let userId = nameAndId.split('/')[1];
        let fileName = nameAndId.split('/')[2];

        let pathToFile = pathToFilesDirectory+'/'+userId+'/'+fileName+'.jpg';
        console.log("path to file = ", pathToFile);
        checkFileExists(pathToFile).then(doesFileExist => {
            if(doesFileExist){
                console.log("yes");
                fs.unlink(pathToFile, (err) => {
                    if(err){
                        reject(err);
                    }
                    resolve(true);
                });
            } else {
                console.log("not");
                resolve(false);
            }
        }).catch(err => {
           reject(err);
        });
    });
};

exports.removeFileByLink = removeFileByLink;

const removeFilesByLinksArr = function (linksArr) {
    return new Promise((resolve, reject)=>{
        linksArr.forEach((currentLink, i)=>{
            removeFileByLink(currentLink).then(result => {
                if(linksArr.length - 1 === i){
                    resolve(true);
                }
            }).catch(err => {
                reject(err);
            });
        });
    });
};

exports.removeFilesByLinksArr = removeFilesByLinksArr;

const removeXMLfileByLink = function(link){
    const pathToCardPatternsDir = paths.pathToCardPatternsDirectory;
    return new Promise((resolve,reject) => {
        let fileName = link.split('getPattern/')[1];

        let pathToFile = pathToCardPatternsDir+'/'+fileName+'.xml';
        console.log("path to file = ", pathToFile);
        checkFileExists(pathToFile).then(doesFileExist => {
            if(doesFileExist){
                fs.unlink(pathToFile, (err) => {
                    if(err){
                        reject(err);
                    }
                    resolve(true);
                });
            } else {
                resolve(false);
            }
        }).catch(err => {
            reject(err);
        });
    });
};

exports.removeXMLfileByLink = removeXMLfileByLink;

const removeXMLfilesByLinksArr = function (linksArr) {
    return new Promise((resolve, reject)=>{
        linksArr.forEach((currentLink, i)=>{
            removeXMLfileByLink(currentLink).then(result => {
                if(linksArr.length - 1 === i){
                    resolve(true);
                }
            }).catch(err => {
                reject(err);
            });
        });
    });
};

exports.removeXMLfilesByLinksArr = removeXMLfilesByLinksArr;

const writeFile = function(fileBuffer, pathToFile){
    return new Promise((resolve,reject) => {
        fs.writeFile(pathToFile, fileBuffer, (err) => {
            if(err){
                reject(err);
            } else {
                resolve(true);
            }
        });
    });
};

exports.writeFile = writeFile;








const getFileExtensionFromFileName = function(fileName){

    let extension = fileName.split('.')[1];

    let ext = extension.toLowerCase();
    return '.'+ext;
};

exports.getFileExtensionFromFileName = getFileExtensionFromFileName;

const getFileTypeByExtension = function(extension){

    const videoExtensionsArr = ['.ogv', '.avi', '.3gp', '.flv', '.webm', '.mp4'];
    const imageExtensionsArr = ['.jpg', '.png', '.jpeg', '.tiff'];

    let idx = videoExtensionsArr.indexOf(extension);

    if(idx !== -1){
        return 'video';
    } else {
        let idx = imageExtensionsArr.indexOf(extension);

        if(idx !== -1){
            return 'image';
        }
    }
};

exports.getFileTypeByExtension = getFileTypeByExtension;



const removeDir = function(pathToDir){
    return new Promise((resolve,reject) => {
        fs.access(pathToDir, (err) =>{
            if(err) {
                reject('dir doesnot exists')
            } else {
                rmdir(pathToDir, (err, dirs, files) => {
                    if(err) { reject(err); }
                    resolve(true);
                });
            }
        });
    });
};
exports.removeDir = removeDir;