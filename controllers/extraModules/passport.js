const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const usersRepo = require('./../../database/repositories/usersRepo');
const adminsRepo = require('./../../database/repositories/adminsRepo');
const jwt = require('jsonwebtoken');
const util = require('util');

// Setup work and export for the JWT passport strategy
module.exports = (passport) => {
  const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
    secretOrKey: process.env.JWT_AUTH_SECRET
  };

  passport.use('jwt', new JwtStrategy(opts, (jwt_payload, done) => {
/*      console.log("before verify ! ");
      console.log("jwt_payload = "+util.inspect(jwt_payload));
      console.log("opts = "+util.inspect(opts.jwtFromRequest));
      console.log("exp date = "+util.inspect(new Date(jwt_payload.exp * 1000).getTime()));
      console.log("date now = "+new Date().getTime());*/

      let id = jwt_payload.id;
      let email = jwt_payload.email;

      usersRepo.verifyEmailAndId(email, id).then(user => {
          let data = {
              email: user.email,
              id: user.id,
              role : 'user'
          };
          done(null, data);
      }).catch(err => {
          done({status : 401, error : err});
      });
  }));

  passport.use('admin-jwt', new JwtStrategy(opts, (jwt_payload, done) => {
      let id = jwt_payload.id;
      let email = jwt_payload.email;

      console.log("jwt payload = ", util.inspect(jwt_payload));
      adminsRepo.verifyEmailAndId(email, id).then(admin => {
          let data = {
              email: admin.email,
              id: admin.id,
              role : 'admin'
          };
          done(null, data);
      }).catch(err => {
          done({status : 401, error : err});
      });
  }));
};
