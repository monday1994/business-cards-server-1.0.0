const util = require('util');
const Promise = require('bluebird');
const crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = process.env.CRYPTO_PASSWORD;

function encrypt(text){
    return new Promise((resolve, reject) => {
        try{
            let cipher = crypto.createCipher(algorithm, password);
            let encryptedText = cipher.update(text,'utf8','hex');
            encryptedText += cipher.final('hex');
            resolve(encryptedText);
        } catch(err){
            console.log("error in encrypt");
            reject('string: '+text+', cannot be encrypted');
        }
    });
}

exports.encrypt = encrypt;

function decrypt(text){
    return new Promise((resolve, reject) => {
        try{
            let decipher = crypto.createDecipher(algorithm,password);
            let decryptedText = decipher.update(text,'hex','utf8');
            decryptedText += decipher.final('utf8');
            resolve(decryptedText);
        } catch(err){
            console.log("error in decrypt");
            reject({
                message : 'string: '+text+', cannot be decrypted'
            });
        }
    });
}

exports.decrypt = decrypt;
