const express = require('express');
let router = express.Router();
const usersRepo = require('../database/repositories/usersRepo');
const util = require('util');
const multer = require('multer');
const upload = multer({ errorHandling : 'automatic' });
const sequelize = require('../database/relationalDB').sequelize;

/* GET home page. */
router.get('/', (req, res, next) => {

    res.json({
        message : 'server works perfectly'
    });

});

router.get('/sockets', (req,res,next) => {

    console.log("token on server side = ", util.inspect(req.query.token));

    res.render('sockets_test.ejs', {
        serverUrl : process.env.server_url,
        token : req.query.token
    });
});

module.exports = router;