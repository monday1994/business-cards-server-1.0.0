const util = require('util');

exports.checkFormDataContentType = function(req){
    return req.headers['content-type'].startsWith('multipart/form-data');
};

exports.checkJsonContentType = function(req){
    return req.headers['content-type'] === 'application/json';
};