const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const VMT = require('../../configuration/validationMessagesTemplates');
const util = require('util');
const headersValidator = require('./headersValidator');
const async = require('async');

exports.validateCreateBusinessCard = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('CompanyDetailsId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        let colorsArr = req.body.colors;

        validateArrayOfColors(colorsArr).then(result => {
            req.getValidationResult().then(result => {
                if(!result.isEmpty()) {
                    reject(util.inspect(result.array()));
                }
                resolve(true);
            });
        }).catch(err => {
            reject(err);
        });
    });
};

exports.validateUpdateBusinessCard = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.checkBody('CompanyDetailsId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        let colorsArr = req.body.colors;

        validateArrayOfColors(colorsArr).then(result => {
            req.getValidationResult().then(result => {
                if(!result.isEmpty()) {
                    reject(util.inspect(result.array()));
                }
                resolve(true);
            });
        }).catch(err => {
            reject(err);
        });
    });
};

exports.validateSetPattern = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('businessCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});
        req.checkBody('patternId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};


exports.validateGetBusinessCardContacts = function (req) {
    return new Promise((resolve, reject) => {

        req.checkQuery('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateDeleteById = function (req) {
    return new Promise((resolve, reject) => {
        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

const validateArrayOfColors = function(colorsArr){
    return new Promise((resolve, reject)=>{
        if(Array.isArray(colorsArr)){
            async.each(colorsArr, (currentArrElem, cb)=>{
                if(Object.keys(currentArrElem).length === 2){
                    if(regex.hexadecimalColorRegex.test(currentArrElem['color']) && currentArrElem['color'].length === 7){
                        if(currentArrElem['fullfilment'] > 0 && currentArrElem['fullfilment'] < 100){
                            cb(null);
                        } else {
                            return cb('fullfilment must be greater than 0 and less than 100');
                        }
                    } else {
                        return cb('color must match pattern : "#112233"');
                    }
                } else {
                    return cb('colors array elem : '+currentArrElem+' must contains 2 elements : color and fullfilment')
                }
            }, err =>{
                if(err){
                    reject(err);
                } else {
                    resolve(true);
                }
            })
        } else {
            reject('colors is not an array');
        }
    });
};