const util = require('util');
const regex = require('../../controllers/extraModules/regex');
const isBuffer = require('is-buffer');
const async = require('async');
const imagesMimetypes = ['image/jpeg', 'image/png'];
const xmlMimeType = ['text/xml'];

const validateFile = function(file){
    return new Promise((resolve,reject)=>{
        if(file){
            let buffer = file.buffer;
            let mimetype = file.mimetype;

            if(!isBuffer(buffer)){
                reject('sent file buffer is not a valid buffer');
            }

            let isMimeTypeCorrect = imagesMimetypes.indexOf(mimetype);

            if(isMimeTypeCorrect === -1){
                reject('mimetype '+mimetype+' of sent file is not allowed by business cards');
            }

            resolve({
                buffer : buffer,
                mimetype : mimetype
            });
        } else {
            reject('file has not been sent');
        }
    });
};

exports.validateFile = validateFile;

exports.validateMultipleFiles = function(filesArr){
    return new Promise((resolve,reject) => {
        let arrToBeReturn = [];
        if(Array.isArray(filesArr) && filesArr.length > 0){
            async.each(filesArr, ((currentFile, cb)=>{
                validateFile(currentFile).then(validatedFile => {
                    arrToBeReturn.push(validatedFile);
                    cb(null);
                }).catch(err => {
                    return cb(err);
                });
            }), (err => {
                if(err){
                    reject(err);
                } else {
                    resolve(arrToBeReturn);
                }
            }));
        } else {
           reject('sent files are not an array');
        }
    });
};

const validateXMLfile = function(file){
    return new Promise((resolve,reject)=>{
        if(file){
            let buffer = file.buffer;
            let mimetype = file.mimetype;

            if(!isBuffer(buffer)){
                reject('sent file buffer is not a valid buffer');
            }

            let isMimeTypeCorrect = xmlMimeType.indexOf(mimetype);

            if(isMimeTypeCorrect === -1){
                reject('mimetype '+mimetype+' of sent file is not an xml');
            }

            resolve({
                buffer : buffer,
                mimetype : mimetype
            });
        } else {
            reject('file has not been sent');
        }
    });
};

exports.validateXMLfile = validateXMLfile;

exports.validateMultipleXMLfiles = function(filesArr){
    return new Promise((resolve,reject) => {
        let arrToBeReturn = [];
        if(Array.isArray(filesArr) && filesArr.length > 0){
            async.each(filesArr, ((currentFile, cb)=>{
                validateXMLfile(currentFile).then(validatedFile => {
                    arrToBeReturn.push(validatedFile);
                    cb(null);
                }).catch(err => {
                    return cb(err);
                });
            }), (err => {
                if(err){
                    reject(err);
                } else {
                    if(arrToBeReturn.length === 2){
                        resolve(arrToBeReturn);
                    } else {
                        reject('Num of xml files must be 2');
                    }
                }
            }));
        } else {
            reject('sent files is not an array');
        }
    });
};