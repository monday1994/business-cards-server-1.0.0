const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const VMT = require('../../configuration/validationMessagesTemplates');
const util = require('util');
const headersValidator = require('./headersValidator');

exports.validateCreateCompanyDetails = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('role', 'Invalid role - must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('website', VMT.INVALID_WEBSITE_MESSAGE).matches(regex.websiteRegex).isLength({
            min : VP.WEBSITE_MIN_LENGTH,
            max : VP.WEBSITE_MAX_LENGTH
        });

        req.checkBody('phone', VMT.INVALID_PHONE_NUMBER).matches(regex.polishPhoneNumber);

        req.checkBody('address', 'Invalid address, must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.ADDRESS_MIN_LENGTH,
            max : VP.ADDRESS_MAX_LENGTH
        });

        req.checkBody('description', 'Invalid description, must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.DESCRIPTION_MIN_LENGTH,
            max : VP.DESCRIPTION_MAX_LENGTH
        });

        if(req.body.KRS || req.body.REGON || req.body.NIP){
            if(req.body.KRS){
                req.checkBody('KRS', VMT.INVALID_KRS_MESSAGE).matches(regex.digitsRegex).isLength({
                    min : VP.KRS_LENGTH,
                    max : VP.KRS_LENGTH
                });
            }

            if(req.body.NIP){
                req.checkBody('NIP', VMT.INVALID_NIP_MESSAGE).matches(regex.digitsRegex).isLength({
                    min : VP.NIP_LENGTH,
                    max : VP.NIP_LENGTH
                });
            }

            if(req.body.REGON){
                req.checkBody('REGON', VMT.INVALID_REGON_MESSAGE).matches(regex.digitsRegex).isLength({
                    min : VP.REGON_MIN_LENGTH,
                    max : VP.REGON_MAX_LENGTH
                });
            }
        } else {
            reject('One of : NIP, REGON or KRS must be specified');
        }

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateUpdateCompanyDetails = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('role', 'Invalid role - must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('website', VMT.INVALID_WEBSITE_MESSAGE).matches(regex.websiteRegex).isLength({
            min : VP.WEBSITE_MIN_LENGTH,
            max : VP.WEBSITE_MAX_LENGTH
        });

        req.checkBody('phone', VMT.INVALID_PHONE_NUMBER).matches(regex.polishPhoneNumber);

        req.checkBody('address', 'Invalid address, must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.ADDRESS_MIN_LENGTH,
            max : VP.ADDRESS_MAX_LENGTH
        });

        req.checkBody('description', 'Invalid description, must be string').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.DESCRIPTION_MIN_LENGTH,
            max : VP.DESCRIPTION_MAX_LENGTH
        });

        if(req.body.KRS || req.body.REGON || req.body.NIP){
            if(req.body.KRS){
                req.checkBody('KRS', VMT.INVALID_KRS_MESSAGE).matches(regex.digitsRegex).isLength({
                    min : VP.KRS_LENGTH,
                    max : VP.KRS_LENGTH
                });
            }

            if(req.body.NIP){
                req.checkBody('NIP', VMT.INVALID_NIP_MESSAGE).matches(regex.digitsRegex).isLength({
                    min : VP.NIP_LENGTH,
                    max : VP.NIP_LENGTH
                });
            }

            if(req.body.REGON){
                req.checkBody('REGON', VMT.INVALID_REGON_MESSAGE).matches(regex.digitsRegex).isLength({
                    min : VP.REGON_MIN_LENGTH,
                    max : VP.REGON_MAX_LENGTH
                });
            }
        } else {
            reject('One of : NIP, REGON or KRS must be specified');
        }

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetCompanyDetailsById = function (req) {
    return new Promise((resolve, reject) => {

        req.checkQuery('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateDeleteById = function (req) {
    return new Promise((resolve, reject) => {
        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};
