const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const VMT = require('../../configuration/validationMessagesTemplates');
const util = require('util');
const headersValidator = require('./headersValidator');

exports.validateCreateRoom = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('creatorBusinessCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('pin', 'Invalid pin - must contain only characters used in writing').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.PIN_MIN_LENGTH,
            max : VP.PIN_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateUpdateRoom = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('pin', 'Invalid pin - must contain only characters used in writing').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.PIN_MIN_LENGTH,
            max : VP.PIN_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateJoinRoom = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('roomId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('businessCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('pin', 'Invalid pin - must contain only characters used in writing').matches(regex.allCharactersUsedInWriting).isLength({
            min : VP.PIN_MIN_LENGTH,
            max : VP.PIN_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateLeaveRoom = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('roomId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetRoomById = function (req) {
    return new Promise((resolve, reject) => {

        req.checkQuery('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateDeleteById = function (req) {
    return new Promise((resolve, reject) => {
        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};
