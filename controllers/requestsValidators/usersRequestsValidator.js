const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const VMT = require('../../configuration/validationMessagesTemplates');
const util = require('util');
const headersValidator = require('./headersValidator');

exports.validateCreateClient = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });
        req.checkBody('lastname', VMT.INVALID_LASTNAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.LASTNAME_MIN_LENGTH,
            max : VP.LASTNAME_MAX_LENGTH
        });
        req.checkBody('email', VMT.INVALID_EMAIL_MESSAGE).matches(regex.emailRegex).isLength({
            min : VP.EMAIL_MIN_LENGTH,
            max : VP.EMAIL_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetProfilePhoto = function(req){
    return new Promise((resolve, reject) => {
        console.log("params = ", req.params);
        req.checkParams('userId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkParams('photoName', VMT.INVALID_PROFILE_PHOTO_NAME).matches(regex.shortId).isLength({
            min : VP.PROFILE_PHOTO_NAME_MIN_LENGTH,
            max : VP.PROFILE_PHOTO_NAME_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateBuyPattern = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('patternId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('stripeToken').matches(regex.stripeToken).isLength({
            min: VP.STRIPE_TOKEN_MIN_LENGTH,
            max : VP.STRIPE_TOKEN_MAX_LENGTH
        });

        req.checkBody('currency').isIn(VP.CURRENCIES_ARR);
        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateAddContact = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('userCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('contactCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateRemoveContact = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('userCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('contactCardId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetById = function (req) {
    return new Promise((resolve, reject) => {

        req.checkQuery('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateSetNotificationAsReadOut = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('notificationId', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};