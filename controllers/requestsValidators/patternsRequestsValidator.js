const regex = require('./../extraModules/regex');
const VP = require('../../configuration/validationParameters');
const VMT = require('../../configuration/validationMessagesTemplates');
const util = require('util');
const headersValidator = require('./headersValidator');

exports.validateCreatePattern = function(req){
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkFormDataContentType(req)){
            reject('invalid header type, must be multipart/form-data');
        }

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('price', VMT.INVALID_PRICE_MESSAGE).isFloat({min : 0.0});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateUpdatePatternData = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.checkBody('name', VMT.INVALID_NAME_MESSAGE).matches(regex.onlyAlphabetLetterRegex).isLength({
            min : VP.NAME_MIN_LENGTH,
            max : VP.NAME_MAX_LENGTH
        });

        req.checkBody('price', VMT.INVALID_PRICE_MESSAGE).isFloat({min : 0.0});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateUpdatePatternFiles = function (req) {
    return new Promise((resolve, reject) => {

        if(!headersValidator.checkFormDataContentType(req)){
            reject('invalid header type, must be multipart/form-data');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetPattern = function (req) {
    return new Promise((resolve, reject) => {

        req.checkParams('patternFileName', VMT.INVALID_PATTERN_NAME).matches(regex.shortId).isLength({
            min : VP.PATTERN_FILE_NAME_MIN_LENGTH,
            max : VP.PATTERN_FILE_NAME_MAX_LENGTH
        });

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateGetPatternById = function (req) {
    return new Promise((resolve, reject) => {

        req.checkQuery('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};

exports.validateDeleteById = function (req) {
    return new Promise((resolve, reject) => {
        if(!headersValidator.checkJsonContentType(req)){
            reject('invalid header type, must be application/json');
        }

        req.checkBody('id', VMT.INVALID_ID_MESSAGE).isInt({min : 1});

        req.getValidationResult().then(result => {
            if(!result.isEmpty()) {
                reject(util.inspect(result.array()));
            }
            resolve(true);
        });
    });
};
