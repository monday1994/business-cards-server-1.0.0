const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const usersRepo = require('../database/repositories/usersRepo');
const patternsRepo = require('../database/repositories/patternsRepo');
const businessCardsRepo = require('../database/repositories/businessCardsRepo');
const notificationsRepo = require('../database/repositories/notificationsRepo');
const requestValidator = require('./requestsValidators/usersRequestsValidator');
const shortId = require('shortid');
const multer = require('multer');
const upload = multer({ errorHandling : 'automatic' });
const multerFilesValidator = require('./requestsValidators/multerFilesValidator');
const imageProcessor = require('./extraModules/imageProcessor');
const config = require('../configuration/config');
const fs = require('fs');
const paths = require('../configuration/paths');
const Promise = require('bluebird');
const filesManager = require('./extraModules/filesManager');
const _ = require('lodash');

/*
    headers : {
        Content-Type : multipart/form-data,
        Authorization : user JWT accessToken
    }
    body : {
        file : 'profile picture in base64'
    }
*/
//todo 27.12.17 - works perfectly
router.post('/setProfilePhoto', upload.single('file'), passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";
    let file = req.file;
    let xDimension = config.profilePictureXdimension;
    let yDimension = config.profilePictureYdimension;
    multerFilesValidator.validateFile(file).then(validatedFile =>{

        imageProcessor.imageMagickResize(validatedFile.buffer, xDimension, yDimension).then(fileToBeSave => {
            let generatedFileName = shortId.generate();
            let pathToUserImagesDirectory = paths.pathToUsersImagesDirectory+'/'+req.user.id+'/'+generatedFileName+'.jpg';
            let linkToProfilePhoto = paths.linkToProfilePhoto+'/'+req.user.id+'/'+generatedFileName;

            fs.writeFile(pathToUserImagesDirectory, fileToBeSave, (err,result)=>{
                if(err){
                    return next({status : 400, error : err});
                } else {
                    let userToBeUpdate = {
                        id : req.user.id,
                        linkToProfilePhoto : linkToProfilePhoto
                    };

                    usersRepo.getByEmail(req.user.email).then(userFromDb => {
                        let plainUserFromDb = userFromDb.get({plain:true});

                        usersRepo.update(userToBeUpdate).then(updatedUser => {
                            if(plainUserFromDb.linkToProfilePhoto){
                                filesManager.removeFileByLink(plainUserFromDb.linkToProfilePhoto).then(result => {
                                    res.json({
                                        linkToProfilePhoto : linkToProfilePhoto
                                    });
                                }).catch(err => {
                                    return next({status : 400, error : err});
                                })
                            } else {
                                res.json({
                                    linkToProfilePhoto : linkToProfilePhoto
                                });
                            }
                        }).catch(err => {
                            return next({status : 400, error : err});
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    });
                }
            })
        }).catch(err =>{
            return next({status : 400, error : err});
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {
    }
    params : {
        userId : 1,
        photoName : '12awd23-aw'
    }
*/
//todo 27.12.17 - works perfectly
router.get('/getProfilePhoto/:userId/:photoName', (req,res,next) => {
    requestValidator.validateGetProfilePhoto(req).then(result => {
        let userId = req.params.userId;
        let photoName = req.params.photoName;

        let pathToPhoto = paths.pathToUsersImagesDirectory+'/'+userId+'/'+photoName+'.jpg';

        filesManager.doesFileExists(pathToPhoto).then(result => {
            res.sendFile(pathToPhoto);
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        patternId : 1,
        stripeToken : 'token used for end the payment process',
        currency : 'gbp,usd,pln,eur'
    }
*/
//todo 27.12.17 - works perfectly
router.post('/buyPattern', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateBuyPattern(req).then(result => {
        let patternId = req.body.patternId;
        let userId = req.user.id;
        let stripeToken = req.body.stripeToken;
        let currency = req.body.currency;

        Promise.all([
            patternsRepo.getById(patternId),
            usersRepo.getById(userId),
            usersRepo.getUserPatterns(userId)
        ]).spread((pattern, user, userPatterns) => {
            let isPatternAlreadyBought = _.find(userPatterns, {PatternId : patternId});

            if(!isPatternAlreadyBought){
                //todo add here stripe transcation
                //stripeApi.handleCharge(stripeToken, pattern.price, currency, user.name+' '+user.lastname+' bought pattern business card').then(result =>{
                    user.addPatterns(pattern).then(result => {
                        res.json({
                            boughtPattern : pattern,
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    });
                /*}).catch(err => {
                    return next({status : 400, error : err});
                });   */
            } else {
                return next({status : 409, error : 'Pattern with id : '+patternId+' has been already bought by user'});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userCardId : 1,
        contactCardId : 2
    }
*/
//todo 27.12.17 - works perfectly
router.post('/addContact', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateAddContact(req).then(result => {
        let userCardId = req.body.userCardId;
        let contactCardId = req.body.contactCardId;

        Promise.all([
            businessCardsRepo.getById(userCardId),
            businessCardsRepo.getById(contactCardId)
        ]).spread((userCard, contactCard) => {

            Promise.all([
                userCard.addContacts(contactCard),
                contactCard.addContacts(userCard)
            ]).spread((firstResult,secondResult) => {
                console.log("first result = ", util.inspect(firstResult));
                console.log("second result = ", util.inspect(secondResult));
                if(firstResult.length && secondResult.length){
                    res.json({
                        message : 'Cards have been exchanged successfully'
                    });
                } else {
                    res.json({
                        message : 'Cards have not been exchanged, they probably have been exchanged before'
                    });
                }
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });

    }).catch(err => {
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        userCardId : 1,
        contactCardId : 2
    }
*/
//todo 27.12.17 - works perfectly
router.post('/removeContact', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateAddContact(req).then(result => {
        let userCardId = req.body.userCardId;
        let contactCardId = req.body.contactCardId;

        Promise.all([
            businessCardsRepo.getById(userCardId),
            businessCardsRepo.getById(contactCardId)
        ]).spread((userCard, contactCard) => {

            userCard.removeContacts(contactCard).then(result=>{
                if(result){
                    res.json({
                        message : 'Contact have been successfully removed'
                    });
                } else {
                    res.json({
                        message : 'Contact have not been removed, it probably did not exist'
                    });
                }
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });

    }).catch(err => {
        return next({status : 400, error : err})
    });
});

/*
    headers : {
        Authorization : user JWT accessToken
    }
    query : {
        id : 1,
    }
*/
//todo 27.12.17 - works perfectly
router.get('/getById', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateGetById(req).then(result => {
        let id = req.query.id;
        let userId = req.user.id;

        if(id === userId){
            usersRepo.getById(id).then(user => {
                let plainUser = user.get({plain:true});
                delete plainUser.password;

                res.json({
                    user : plainUser
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });
        } else {
            usersRepo.getUserContacts(id).then(arrOfContacts => {

                let doesUserIsInContacts = _.find(arrOfContacts, { UserId : userId});

                if(doesUserIsInContacts){
                    usersRepo.getById(id).then(user => {
                        let plainUser = user.get({plain:true});
                        delete plainUser.password;

                        res.json({
                            user : plainUser
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    })
                } else {
                    return next({status : 401, error : 'User with id : '+userId+' have no access to user with id : '+id});
                }
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }
    }).catch(err => {
        return next({status : 400, error : err})
    });
});


/*
    headers : {
        Content-Type : application/json
        Authorization : user JWT accessToken
    }
    body : {
        notificationId : 1,
    }
*/
router.post('/setNotificationAsReadOut',  passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateSetNotificationAsReadOut(req).then(result => {
        let notificationId = req.body.notificationId;
        let userId = req.user.id;

        let notificationToBeUpdate = {
            id : notificationId,
            isReadOut : true
        };

        notificationsRepo.update(notificationToBeUpdate, userId).then(updatedNotification => {
            res.json({
                updatedNotification : updatedNotification
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err})
    });
});

module.exports = router;