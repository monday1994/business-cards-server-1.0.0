const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const usersRepo = require('../database/repositories/usersRepo');
const adminsRepo = require('../database/repositories/adminsRepo');
const requestValidator = require('./requestsValidators/authRequestsValidator');
const EMT = require('../configuration/errorsMessagesTemplates');
const JWT_AUTH_SECRET = process.env.JWT_AUTH_SECRET;
const JWT_EXPIRATION_TIME = require('../configuration/config').JWT_EXPIRATION_TIME;
const crypto = require('../controllers/extraModules/crypto');
const shortid = require('shortid');
const UIDGenerator = require('uid-generator');
const uidgen = new UIDGenerator();
const sendgridApi = require('../api/sendgridApi');
const paths = require('../configuration/paths');
const fs = require('fs');

/*
    headers : {
        Content-Type : application/json,
    }
    body : {
        name : 'francis',
        lastname : 'monday',
        email : 'test@gmail.com'
    }

    returns -> new user
*/

router.post('/registration', (req,res,next) => {
    "use strict";

    requestValidator.validateRegistration(req).then(result =>{

        let newUser = {
            name : req.body.name,
            lastname : req.body.lastname,
            email : req.body.email,
            forgotPasswordToken : null,
            forgotPasswordTokenExpiry : null
        };

        let password = shortid.generate();

        crypto.encrypt(password).then(encryptedPassword => {
            newUser.password = encryptedPassword;
            usersRepo.create(newUser).then(user => {

                let plainUser = user.get({plain : true});

                let pathToUserImagesDirectory = paths.pathToUsersImagesDirectory+'/'+plainUser.id;
                fs.mkdir(pathToUserImagesDirectory, ((err, result) => {
                    if(err){
                        return next({status : 400, error : err});
                    } else {
                        sendgridApi.sendEmailWithPasswordToNewUser(plainUser.name, plainUser.lastname, password, plainUser.email).then(result => {
                            delete plainUser.password;
                            res.json({
                                newUser : plainUser
                            });
                        }).catch(err =>{
                            return next({status : 400, error : err});
                        });
                    }
                }));
            }).catch(err => {
                return next({ status : 400, error : err});
            });
        }).catch(err=>{
            return next({ status : 400, error : err })
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
    }
    body : {
        email : 'test@gmail.com',
        password : 'somePass'
    }

    returns -> token, user role and id
*/

router.post('/userLogin', (req,res,next) => {
    "use strict";

    requestValidator.validateLogin(req).then(result =>{
        let email = req.body.email;
        let password = req.body.password;

        usersRepo.validateLogin(email, password).then(user => {
            delete user.password;

            const dataForSign = {
                id : user.id,
                email : user.email,
                role : 'user'
            };

            jwt.sign(dataForSign, JWT_AUTH_SECRET,{ expiresIn: JWT_EXPIRATION_TIME }, (err, token) => {
                if(err){
                    return next({
                        status : 400,
                        error : err
                    });
                }
                res.json({
                    id : user.id,
                    token : token,
                    role : 'user'
                });
            });
        }).catch(err => {
            return next({ status : 400, error : err});
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json,
    }
    body : {
        email : 'test@gmail.com',
        password : 'somePass'
    }

    returns -> token, user role and id
*/

router.post('/adminLogin', (req,res,next) => {
    "use strict";

    requestValidator.validateLogin(req).then(result =>{
        let email = req.body.email;
        let password = req.body.password;

        adminsRepo.validateLogin(email, password).then(admin => {
            delete admin.password;

            const dataForSign = {
                id : admin.id,
                email : admin.email,
                role : 'admin'
            };

            jwt.sign(dataForSign, JWT_AUTH_SECRET,{ expiresIn: JWT_EXPIRATION_TIME }, (err, token) => {
                if(err){
                    return next({
                        status : 400,
                        error : err
                    });
                }
                res.json({
                    id : admin.id,
                    token : token,
                    role : 'admin'
                });
            });
        }).catch(err => {
            return next({ status : 400, error : err});
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});



/*
    headers : {
        Content-Type : application/json,
        Authorization : JWT accessToken
    }
    //both passwords should be encrypted
    body : {
        oldPassword : 'some pass',
        newPassword : 'new pass'
    }
*/

//todo should work 29.11.17
router.post('/changeUserPassword', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateChangePassword(req).then(result =>{

        let oldPassword = req.body.oldPassword;
        let newPassword = req.body.newPassword;

        console.log("user = ", util.inspect(req.user));
        let userEmail = req.user.email;

        usersRepo.validateOldPassword(userEmail, oldPassword).then(result => {

            crypto.decrypt(newPassword).then(decryptedNewPassword => {
                let userToBeUpdate = {
                    id : req.user.id,
                    password : newPassword
                };
                usersRepo.update(userToBeUpdate).then(updatedUser => {
                    let plainUser = updatedUser.get({plain : true});
                    sendgridApi.sendEmailWithChangedPassword(plainUser.name, plainUser.lastname, decryptedNewPassword, plainUser.email).then(result => {
                        res.json({
                            message : 'user password has been changed'
                        });
                    }).catch(err =>{
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });

    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : admin JWT accessToken
    }
    body : {
        oldPassword : 'some pass',
        newPassword : 'new pass'
    }
*/

router.post('/changeAdminPassword', passport.authenticate('admin-jwt', {session: false}), (req,res,next) => {
    "use strict";
    requestValidator.validateChangePassword(req).then(result =>{

        if(req.user.role === 'admin'){
            let oldPassword = req.body.oldPassword;
            let newPassword = req.body.newPassword;

            let adminEmail = req.user.email;
            adminsRepo.validateOldPassword(adminEmail, oldPassword).then(result => {
                crypto.decrypt(newPassword).then(decryptedNewPassword => {
                    let adminToBeUpdate = {
                        id : req.user.id,
                        password : newPassword
                    };
                    adminsRepo.update(adminToBeUpdate).then(updatedAdmin => {
                        let plainAdmin = updatedAdmin.get({plain:true});
                        sendgridApi.sendEmailWithChangedPassword(plainAdmin.name, plainAdmin.lastname, decryptedNewPassword, plainAdmin.email).then(result => {
                            res.json({
                                message : 'admin password has been changed'
                            });
                        }).catch(err =>{
                            return next({status : 400, error : err});
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });
        } else {
            return next({
                status: 401,
                error : EMT.INVALID_TOKEN_ERROR_MESSAGE
            })
        }
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json
    }
    body : {
        email : 'user@gmail.com'
    }
*/

router.post('/forgotUserPassword', (req,res,next) => {
    "use strict";
    requestValidator.validateForgotPassword(req).then(result =>{
        let email = req.body.email;

        usersRepo.getByEmail(email).then(user => {
            uidgen.generate().then(generatedToken => {
                user.forgotPasswordToken = generatedToken;
                user.forgotPasswordTokenExpiry = (new Date().getTime()+Number(process.env.FORGOT_PASSWORD_EXPIRY));

                let forgotPasswordLink = process.env.server_url+'/auth/resetUserPassword/'+generatedToken;
                let plainUser = user.get({plain:true});

                usersRepo.update(plainUser).then(updatedUser => {
                    sendgridApi.sendEmailWithResetPasswordLink(plainUser.name, forgotPasswordLink, plainUser.email).then(result => {
                        res.json({
                            message : 'reset link has been sent via email'
                        });
                    }).catch(err =>{
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({
                    status : 400,
                    error : err
                });
            });

        }).catch(err => {
            return next({
                status : 400,
                error : err
            });
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json
    }
    params : {
        token : 'wdawdj12949813dfahwd78y1278'
    }
*/

router.get('/resetUserPassword/:token', (req,res,next) => {
    requestValidator.validateResetPasswordToken(req).then(result => {
        let token = req.params.token;

        usersRepo.getByForgotPasswordToken(token).then(user => {

            let password = shortid.generate();

            crypto.encrypt(password).then(encryptedPassword => {
                user.password = encryptedPassword;
                user.forgotPasswordTokenExpiry = null;
                user.forgotPasswordToken = null;

                let plainUser = user.get({plain : true});

                usersRepo.update(plainUser).then(updatedUser => {
                    sendgridApi.sendEmailAfterResetPassword(plainUser.name, password, plainUser.email) .then(result => {
                        res.json({
                            message : 'new password has been sent via email'
                        });
                    }).catch(err =>{
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({ status : 400, error : err});
                })
            }).catch(err=>{
                return next({ status : 400, error : err })
            });
        }).catch(err => {
            return next({status : 400, error : err});
        })
    }).catch(err =>{
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json
    }
    body : {
        email : 'user@gmail.com'
    }
*/
router.post('/forgotAdminPassword', (req,res,next) => {
    "use strict";
    requestValidator.validateForgotPassword(req).then(result =>{
        let email = req.body.email;

        adminsRepo.getByEmail(email).then(admin => {
            uidgen.generate().then(generatedToken => {
                admin.forgotPasswordToken = generatedToken;
                admin.forgotPasswordTokenExpiry = (new Date().getTime()+Number(process.env.FORGOT_PASSWORD_EXPIRY));

                //todo remember that localhost should also have port specyfied
                let forgotPasswordLink = process.env.server_url+'/auth/resetAdminPassword/'+generatedToken;

                let plainAdmin = admin.get({plain:true});

                adminsRepo.update(plainAdmin).then(updatedAdmin => {
                    sendgridApi.sendEmailWithResetPasswordLink(plainAdmin.name, forgotPasswordLink, plainAdmin.email).then(result => {
                        res.json({
                            message : 'reset link has been sent via email'
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    });
                }).catch(err =>{
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({
                    status : 400,
                    error : err
                });
            });
        }).catch(err => {
            return next({
                status : 400,
                error : err
            });
        })
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json
    }
    params : {
        token : 'wdawdj12949813dfahwd78y1278'
    }
*/

router.get('/resetAdminPassword/:token', (req,res,next) => {
    requestValidator.validateResetPasswordToken(req).then(result => {
        let token = req.params.token;

        adminsRepo.getByForgotPasswordToken(token).then(admin => {

            let password = shortid.generate();

            crypto.encrypt(password).then(encryptedPassword => {
                admin.password = encryptedPassword;
                admin.forgotPasswordTokenExpiry = null;
                admin.forgotPasswordToken = null;

                let plainAdmin = admin.get({plain : true});

                adminsRepo.update(plainAdmin).then(updatedAdmin => {
                    sendgridApi.sendEmailAfterResetPassword(plainAdmin.name, password, plainAdmin.email) .then(result => {
                        res.json({
                            message : 'new password has been sent via email'
                        });
                    }).catch(err =>{
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({ status : 400, error : err});
                })
            }).catch(err=>{
                return next({ status : 400, error : err })
            });
        }).catch(err => {
            return next({status : 400, error : err});
        })
    }).catch(err =>{
        return next({status : 400, error : err});
    });
});


module.exports = router;