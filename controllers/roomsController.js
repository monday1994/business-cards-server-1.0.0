const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const businessCardsRepo = require('../database/repositories/businessCardsRepo');
const roomsRepo = require('../database/repositories/roomsRepo');
const notificationsRepo = require('../database/repositories/notificationsRepo');
const requestValidator = require('./requestsValidators/roomsRequestsValidator');
const Promise = require('bluebird');
const async = require('async');
const _ = require('lodash');
const arrays = require('./extraModules/arrays');
const sockets = require('../sockets/mainSocketsController');

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        name : 'some awesome company name',
        pin : '1234',
        creatorBusinessCardId : 3
    }
*/
//todo 28.12.17 works perfectly
router.post('/createRoom', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateCreateRoom(req).then(result => {

        let userId = req.user.id;
        let creatorBusinessCardId = req.body.creatorBusinessCardId;
        businessCardsRepo.getByIdWithAssociatedData(creatorBusinessCardId).then(card => {
            if(card.UserId === userId){
                let newRoom = {
                    name : req.body.name,
                    pin : req.body.pin,
                    creator : userId,
                    members : [{UserId : userId, BusinessCardId : creatorBusinessCardId}]
                };

                roomsRepo.create(newRoom).then(createdRoom=>{
                    res.json({
                        createdRoom : createdRoom
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            } else {
                return next({status : 400, error : 'Business card with id : '+creatorBusinessCardId+' does not belong to user with id : '+userId});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        id : 1,
        name : 'some awesome company name',
        pin : '1234'
    }
*/
//todo 28.12.17 works perfectly
router.post('/updateRoom', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateUpdateRoom(req).then(result => {

        let userId = req.user.id;

        let roomToBeUpdate = {
            id : req.body.id,
            name : req.body.name,
            pin : req.body.pin
        };

        roomsRepo.update(roomToBeUpdate, userId).then(updatedRoom=>{
            res.json({
                updatedRoom : updatedRoom
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        roomId : 1,
        businessCardId = 2
        pin : '1234'
    }
*/
//todo 28.12.17 works perfectly
router.post('/joinRoom', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateJoinRoom(req).then(result => {

        let userId = req.user.id;
        let businessCardId = req.body.businessCardId;
        let roomId = req.body.roomId;
        let pin = req.body.pin;

        Promise.all([
            businessCardsRepo.getCardWithContactsById(businessCardId),
            businessCardsRepo.getByIdWithAssociatedPatternAndCompanyDetails(businessCardId),
            roomsRepo.getById(roomId)
        ]).spread((userCardWithContacts, userCardWithPattern, room) => {
            if(userCardWithContacts.UserId === userId){
                if(room.pin === pin){
                    let plainRoom = room.get({plain:true});
                    let parsedMembers = JSON.parse(plainRoom.members);

                    let isUserAlreadyInRoom = _.find(parsedMembers, {UserId : userId});

                    if(!isUserAlreadyInRoom){
                        let plainCard = userCardWithContacts.get({plain : true});
                        let arrOfUserContacts = plainCard.Contacts;

                        async.eachOf(arrOfUserContacts, (currentContact, i, cb) => {
                            arrOfUserContacts[i].ContactId = currentContact.CardCard.ContactId;
                            delete arrOfUserContacts[i].CardCard;
                            cb(null);
                        }, err => {
                            if(err) {
                                return next({status: 400, error: err});
                            } else {
                                let notification = {
                                    UserId : null,
                                    type : 'NEW_CONTACT',
                                    message : "You've got new contact.",
                                    isNew : true,
                                    isReadOut : false,
                                    associatedData : {
                                        NewContactBusinessCard : null
                                    }
                                };

                                async.each(parsedMembers, (currentMember, cb) => {
                                    console.log("contacts = ", util.inspect(arrOfUserContacts));
                                    console.log("members = ", util.inspect(parsedMembers));
                                    let doesMemberIsAlreadyInContactWithUser = _.find(arrOfUserContacts, {ContactId : currentMember.BusinessCardId});

                                    if(doesMemberIsAlreadyInContactWithUser){
                                        console.log("member is already in contact with user, ", util.inspect(doesMemberIsAlreadyInContactWithUser));
                                        cb(null);
                                    } else {
                                        businessCardsRepo.getByIdWithAssociatedPatternAndCompanyDetails(currentMember.BusinessCardId).then(memberBusinessCard => {
                                            Promise.all([
                                                userCardWithContacts.addContacts([currentMember.BusinessCardId]),
                                                memberBusinessCard.addContacts(userCardWithContacts)
                                            ]).then(result => {

                                                let memberNotification = JSON.parse(JSON.stringify(notification));

                                                //notification for user who is joining the room
                                                notification.UserId = userId;
                                                notification.associatedData.NewContactBusinessCard = memberBusinessCard.get({plain : true});

                                                if(sockets.connectedUsers[userId]){
                                                    notification.isNew = false;
                                                    sockets.sendNotification(notification);
                                                    //send noti to request maker
                                                }

                                                //notification for user who is already in room and got new contact
                                                memberNotification.UserId = currentMember.UserId;
                                                memberNotification.associatedData.NewContactBusinessCard = userCardWithPattern.get({plain : true});

                                                if(sockets.connectedUsers[currentMember.UserId]){
                                                    //send noti to member
                                                    memberNotification.isNew = false;
                                                    sockets.sendNotification(memberNotification);
                                                }

                                                Promise.all([
                                                    notificationsRepo.create(notification),
                                                    notificationsRepo.create(memberNotification)
                                                ]).then(result => {
                                                    console.log("contacts have been exchaged ! ");
                                                    cb(null);
                                                }).catch(err => {
                                                    return cb(err);
                                                });
                                            }).catch(err => {
                                                return cb(err);
                                            });
                                        }).catch(err => {
                                            return cb(err);
                                        });
                                    }
                                }, err => {
                                    if(err){
                                        return next({status : 400, error : err});
                                    } else {
                                        parsedMembers.push({
                                            UserId : userId,
                                            BusinessCardId : businessCardId
                                        });
                                        room.members = parsedMembers;

                                        room.update(room.get({plain:true})).then(updatedRoom => {
                                            res.json({
                                                room : updatedRoom
                                            });
                                        }).catch(err => {
                                            return next({status : 400, error : err});
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        return next({status : 400, error : 'User with id : '+userId+' have already joined room with id : '+roomId});
                    }
                } else {
                    return next({status : 401, error : 'Wrong pin'});
                }
            } else {
                return next({ status: 400, error: 'Card with id : ' + businessCardId + ' does not belong to user with id : ' + userId });
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        roomId : 1,
    }
*/
//todo 28.12.17 works perfectly
router.post('/leaveRoom', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateLeaveRoom(req).then(result => {

        let userId = req.user.id;
        let roomId = req.body.roomId;

        roomsRepo.getById(roomId).then(room => {

            let plainRoom = room.get({plain:true});
            let parsedMembers = JSON.parse(plainRoom.members);

            let isUserInRoom = _.find(parsedMembers, {UserId : userId});

            if(isUserInRoom){

                arrays.removeObjectByKey(parsedMembers, {UserId : userId});

                room.members = parsedMembers;

                room.update(room.get({plain:true})).then(updatedRoom => {
                    res.json({
                        message : 'User with id : '+userId+' has successfully left room with id : '+roomId
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                })
            } else {
                return next({status : 400, error : 'User with id : '+userId+' did not join room with id : '+roomId});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });

    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Authorization : user JWT
    }
    query : {
        id : 1
    }
*/
//todo works properly 28.12.17
router.get('/getRoomById', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    requestValidator.validateGetRoomById(req).then(result => {
        let roomId = req.query.id;
        let userId = req.user.id;

        roomsRepo.getById(roomId).then(room => {
            //todo check access
        }).catch(err=>{
            return next({status : 400, error : err});
        })
    }).catch(err =>{
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        content-type : 'application/json'
        Authorization : user JWT
    }
    body : {
        id : 1
    }
*/
//todo test
router.post('/deleteById', passport.authenticate('jwt', {session: false}), (req,res,next) => {

    requestValidator.validateDeleteById(req).then(result => {
        let id = req.body.id;
        let userId = req.user.id;

        roomsRepo.getById(id).then(roomToBeRemoved => {
            if(roomToBeRemoved.creator === userId){
                roomsRepo.removeById(id).then(result => {
                    res.json({
                        message : 'Room with id : '+id+' has been deleted'
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                })
            } else {
                return next({status :401, error : 'Room can be delete only by it creator'});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });

    }).catch(err =>{
        return next({status : 400, error : err});
    })
});


module.exports = router;