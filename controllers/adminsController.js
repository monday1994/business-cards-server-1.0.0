const express = require('express');
let router = express.Router();
const passport = require('passport');
const adminsRepo = require('../database/repositories/adminsRepo');
const requestValidator = require('./requestsValidators/adminsRequestsValidator');
const crypto = require('../controllers/extraModules/crypto');
const shortId = require('shortid');
const EMT = require('../configuration/errorsMessagesTemplates');
const sendgridApi = require('../api/sendgridApi');

/*
    headers : {
        Content-Type : application/json,
        Authorization : admin JWT accessToken
    }
    body : {
        name : 'user'
        lastname : 'userlastname'
        email : 'test@gmail.com',
        password : 'somepass',
        phone : '515-689-233'
        location : 'cracow'
    }
*/
//todo should work 29.11.17
router.post('/createAdmin', passport.authenticate('admin-jwt', {session: false}), (req,res,next) => {
    "use strict";

    if(req.user.role === 'admin'){
        requestValidator.validateCreateAdmin(req).then(result =>{

            let newAdmin = {
                name : req.body.name,
                lastname : req.body.lastname,
                email : req.body.email,
                phone : req.body.phone,
                location : req.body.location
            };

            let password = shortId.generate();

            crypto.encrypt(password).then(encryptedPassword => {
                newAdmin.password = encryptedPassword;

                adminsRepo.create(newAdmin).then((createdAdmin) => {
                    delete createdAdmin.get({plain : true}).password;

                    sendgridApi.sendEmailWithPasswordToNewAdmin(newAdmin.name, newAdmin.lastname, password, newAdmin.email).then(result => {
                        res.json({
                            createdAdmin : createdAdmin
                        });
                    }).catch(err =>{
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err =>{
                return next({
                    status : 400,
                    error : err
                });
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    } else {
        return next({status : 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE});
    }
});



module.exports = router;