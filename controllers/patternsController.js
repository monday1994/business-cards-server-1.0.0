const express = require('express');
let router = express.Router();
const util = require('util');
const passport = require('passport');
const patternsRepo = require('../database/repositories/patternsRepo');
const usersRepo = require('../database/repositories/usersRepo');
const notificationsRepo = require('../database/repositories/notificationsRepo');
const requestValidator = require('./requestsValidators/patternsRequestsValidator');
const shortId = require('shortid');
const EMT = require('../configuration/errorsMessagesTemplates');
const multer = require('multer');
const upload = multer({ errorHandling : 'automatic' });
const multerFilesValidator = require('./requestsValidators/multerFilesValidator');
const paths = require('../configuration/paths');
const Promise = require('bluebird');
const filesManager = require('./extraModules/filesManager');
const async = require('async');
const sockets = require('../sockets/mainSocketsController');

/*
    headers : {
        Content-Type : multipart/form-data,
        Authorization : admin JWT accessToken
    }
    body : {
        name : 'some awesome pattern',
        price : 22.0,
        files : ['obvers in base64', 'revers in base64']
    }
*/
//todo 28.12.17 works perfectly
router.post('/createPattern', upload.array('files', 2), passport.authenticate('admin-jwt', {session: false}), (req,res,next) => {
    "use strict";

    if(req.user.role === 'admin'){

        requestValidator.validateCreatePattern(req).then(result => {
            let files = req.files;
            let newPattern = {
                name : req.body.name,
                price : req.body.price,
                linkToObvers : null,
                linkToRevers : null
            };

            multerFilesValidator.validateMultipleXMLfiles(files).then(validatedFiles =>{
                console.log("validated files lenght = ", util.inspect(validatedFiles.length));
                async.eachOf(validatedFiles, (currentFile, i, cb) => {
                    console.log("i = ", i);
                    if(i === 0){
                        let generatedFileName = 'obvers_'+shortId.generate();
                        let pathToObversCardsPatternsDir = paths.pathToCardPatternsDirectory+'/'+generatedFileName+'.xml';
                        let linkToCardObvers = paths.linkToCardPattern+'/'+generatedFileName;

                        filesManager.writeFile(currentFile.buffer, pathToObversCardsPatternsDir).then(result => {
                            newPattern.linkToObvers = linkToCardObvers;
                            cb(null);
                        }).catch(err => {
                            return cb(err);
                        })
                    } else {
                        let generatedFileName = 'revers_'+shortId.generate();
                        let pathToReversCardsPatternsDir = paths.pathToCardPatternsDirectory+'/'+generatedFileName+'.xml';
                        let linkToCardRevers = paths.linkToCardPattern+'/'+generatedFileName;

                        filesManager.writeFile(currentFile.buffer, pathToReversCardsPatternsDir).then(result => {
                            newPattern.linkToRevers = linkToCardRevers;
                            cb(null);
                        }).catch(err => {
                            return cb(err);
                        });
                    }
                }, err => {
                    if(err){
                        return next({status : 400, error : err});
                    } else {

                        Promise.all([
                            patternsRepo.create(newPattern),
                            usersRepo.getAllIds()
                        ]).spread((createdPattern, usersIds) => {

                            let notification = {
                                UserId : null,
                                type : 'NEW_PATTERN',
                                message : 'New business card pattern available',
                                isNew : true,
                                isReadOut : false,
                                associatedData : {
                                    newPattern : createdPattern.get({plain : true})
                                }
                            };

                            //creating and sending notifs
                            async.each(usersIds, (currentUser, cb) => {

                                notification.UserId = currentUser.id;

                                if(sockets.connectedUsers[currentUser.id]){
                                    notification.isNew = false;
                                    sockets.sendNotification(notification);
                                    console.log("noti has been send");
                                }

                                notificationsRepo.create(notification).then(result => {
                                    console.log("noti has been created");
                                    cb(null);
                                }).catch(err => {
                                    return cb(err);
                                });
                            }, err => {
                                if(err){
                                    return next({status : 400, error : err});
                                } else {
                                    res.json({
                                        createdPattern : createdPattern
                                    });
                                }
                            });
                        }).catch(err => {
                            return next({status : 400, error : err});
                        });
                    }
                });
            }).catch(err =>{
                return next({status : 400, error : err});
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    } else {
        return next({status : 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE})
    }
});


/*
    headers : {
        Content-Type : application/json,
        Authorization : admin JWT accessToken
    }
    body : {
        id : 1,
        name : 'some awesome pattern',
        price : 22.0,
    }
*/
//todo 28.12.17 works perfectly
router.post('/updatePatternData', passport.authenticate('admin-jwt', {session: false}), (req,res,next) => {
    "use strict";

    if(req.user.role === 'admin'){

        requestValidator.validateUpdatePatternData(req).then(result => {

            let patternToBeUpdate = {
                id : req.body.id,
                name : req.body.name,
                price : req.body.price
            };

            patternsRepo.update(patternToBeUpdate).then(updatedPattern => {
                res.json({
                    updatedPattern : updatedPattern
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });

        }).catch(err => {
            return next({status : 400, error : err});
        });
    } else {
        return next({status : 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE})
    }
});


/*
    headers : {
        Content-Type : multipart/form-data,
        Authorization : admin JWT accessToken
    }
    body : {
        id : 1,
        files : ['obvers in base64', 'revers in base64']
    }
*/
//todo 28.12.17 works perfectly
router.post('/updatePatternFiles', upload.array('files', 2), passport.authenticate('admin-jwt', {session: false}), (req,res,next) => {
    "use strict";

    if(req.user.role === 'admin'){

        requestValidator.validateUpdatePatternFiles(req).then(result => {
            let files = req.files;
            let patternToBeUpdate = {
                id : req.body.id,
                name : req.body.name,
                linkToObvers : null,
                linkToRevers : null
            };

            patternsRepo.getById(patternToBeUpdate.id).then(oldPatternFromDb => {
                let plainOldPattern = oldPatternFromDb.get({plain : true});

                let filesToBeRemoveArr = [plainOldPattern.linkToObvers, plainOldPattern.linkToRevers];

                filesManager.removeXMLfilesByLinksArr(filesToBeRemoveArr).then(result => {
                    multerFilesValidator.validateMultipleXMLfiles(files).then(validatedFiles =>{
                        async.eachOf(validatedFiles, (currentFile, i, cb) => {
                            if(i === 0){
                                let generatedFileName = 'obvers_'+shortId.generate();
                                let pathToObversCardsPatternsDir = paths.pathToCardPatternsDirectory+'/'+generatedFileName+'.xml';
                                let linkToCardObvers = paths.linkToCardPattern+'/'+generatedFileName;

                                filesManager.writeFile(currentFile.buffer, pathToObversCardsPatternsDir).then(result => {
                                    patternToBeUpdate.linkToObvers = linkToCardObvers;
                                    cb(null);
                                }).catch(err => {
                                    return cb(err);
                                })
                            } else {
                                let generatedFileName = 'revers_'+shortId.generate();
                                let pathToReversCardsPatternsDir = paths.pathToCardPatternsDirectory+'/'+generatedFileName+'.xml';
                                let linkToCardRevers = paths.linkToCardPattern+'/'+generatedFileName;

                                filesManager.writeFile(currentFile.buffer, pathToReversCardsPatternsDir).then(result => {
                                    patternToBeUpdate.linkToRevers = linkToCardRevers;
                                    cb(null);
                                }).catch(err => {
                                    return cb(err);
                                });
                            }
                        }, err => {
                            if(err){
                                return next({status : 400, error : err});
                            } else {
                                patternsRepo.update(patternToBeUpdate).then(updatedPattern => {
                                    res.json({
                                        updatedPattern : updatedPattern
                                    });
                                }).catch(err => {
                                    return next({status : 400, error : err});
                                });
                            }
                        });
                    }).catch(err =>{
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                })
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    } else {
        return next({status : 401, error : EMT.INVALID_TOKEN_ERROR_MESSAGE})
    }
});


/*
    headers : {
    }
    params : {
        patternFileName : 'revers_1_ryxQ9WHf7f'
    }
*/
//todo works properly 28.12.17
router.get('/getPattern/:patternFileName', (req,res,next) => {
    requestValidator.validateGetPattern(req).then(result => {
        let fileName = req.params.patternFileName;

        let pathToFile = paths.pathToCardPatternsDirectory+'/'+fileName+'.xml';

        filesManager.doesFileExists(pathToFile).then(result => {
            res.sendFile(pathToFile);
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});


/*
    headers : {
    }
    query : {
        id : 1
    }
*/
//todo works properly 28.12.17
router.get('/getPatternById', (req,res,next) => {
    requestValidator.validateGetPatternById(req).then(result => {
        let id = req.query.id;

        patternsRepo.getById(id).then(pattern => {
            res.json({
                pattern : pattern
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});

/*
    headers : {
    }
    query : {
    }
*/
//todo works properly 28.12.17
router.get('/getAllPatterns', (req,res,next) => {
    patternsRepo.getAll().then(patterns => {
        res.json({
            patterns : patterns
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Content-Type : application/json
        Authorization : Admin JWT
    }
    body : {
        id : 1
    }
*/
router.post('/deleteById', passport.authenticate('admin-jwt', {session: false}), (req,res,next) => {

    requestValidator.validateDeleteById(req).then(result => {
        let id = req.body.id;

        patternsRepo.getByIdWithAssociatedUsers(id).then(pattern => {
            let plainPattern = pattern.get({plain : true});

            if(plainPattern.Users.length === 0){
                let filesToBeRemoveArr = [plainPattern.linkToObvers, plainPattern.linkToRevers];

                filesManager.removeXMLfilesByLinksArr(filesToBeRemoveArr).then(result => {
                    patternsRepo.removeById(id).then(result => {
                        res.json({
                            message : 'pattern with id : '+id+' has been deleted',
                            result : result
                        });
                    }).catch(err => {
                        return next({status : 400, error : err});
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            } else {
                return next({status : 409, error : 'Pattern is associated with users therefore it cannot be deleted'});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});



module.exports = router;