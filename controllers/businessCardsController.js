const express = require('express');
let router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const businessCardsRepo = require('../database/repositories/businessCardsRepo');
const companiesDetailsRepo = require('../database/repositories/companiesDetailsRepo');
const patternsRepo = require('../database/repositories/patternsRepo');
const usersRepo = require('../database/repositories/usersRepo');
const requestValidator = require('./requestsValidators/businessCardsRequestsValidator');
const Promise = require('bluebird');
const async = require('async');
const _ = require('lodash');

/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        colors : JSON({color : #123123, fullfilment : 30})
        CompanyDetailsId : 1
    }
*/
//todo 28.12.17 works perfectly
router.post('/createBusinessCard', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateCreateBusinessCard(req).then(result => {

        let userId = req.user.id;

        let newBusinessCard = {
            colors : JSON.stringify(req.body.colors),
            CompanyDetailsId : req.body.CompanyDetailsId,
            UserId : userId
        };

        Promise.all([
            usersRepo.getById(userId),
            companiesDetailsRepo.getById(newBusinessCard.CompanyDetailsId)
        ]).spread((user, companyDetails) => {
            businessCardsRepo.create(newBusinessCard).then(createdBusinessCard => {
                Promise.all([
                    createdBusinessCard.setCompanyDetails(companyDetails),
                    user.addBusinessCard(createdBusinessCard)
                ]).then(result => {
                    res.json({
                        createdBusinessCard : createdBusinessCard
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        id : 1,
        colors : JSON({color : #123123, fullfilment : 30})
        CompanyDetailsId : 1
    }
*/
//todo 28.12.17 works perfectly
router.post('/updateBusinessCard', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateUpdateBusinessCard(req).then(result => {

        let businessCardToBeUpdate = {
            id : req.body.id,
            colors : JSON.stringify(req.body.colors),
            CompanyDetailsId : req.body.CompanyDetailsId
        };

        companiesDetailsRepo.getById(businessCardToBeUpdate.CompanyDetailsId).then(companyDetails => {
            businessCardsRepo.update(businessCardToBeUpdate).then(updatedBusinessCard => {
                updatedBusinessCard.setCompanyDetails(companyDetails).then(result => {
                    res.json({
                        updatedBusinessCard : updatedBusinessCard
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }).catch(err => {
                return next({status : 400, error : err});
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});


/*
    headers : {
        Content-Type : application/json,
        Authorization : user JWT accessToken
    }
    body : {
        businessCardId : 1,
        patternId : 2
    }
*/

//todo should work perfectly 28.12.18
router.post('/setPattern', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    "use strict";

    requestValidator.validateSetPattern(req).then(result => {

        let businessCardId = req.body.businessCardId;
        let patternId = req.body.patternId;
        let userId = req.user.id;

        Promise.all([
            businessCardsRepo.getById(businessCardId),
            usersRepo.getUserPatterns(userId),
            patternsRepo.getById(patternId)
        ]).spread((businessCard, boughtPatterns, pattern) => {
            let doesUserBoughtPattern = _.find(pattern, {id : patternId});

            if(doesUserBoughtPattern && businessCard.UserId === userId){
                businessCard.setPattern(pattern).then(result => {
                    res.json({
                        message : 'Pattern with id : '+patternId+' has been associated with business card - id : '+businessCardId
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                })
            } else {
                return next({status : 400, error : 'User with id : '+userId+' did not buy pattern or business card does not belongs to him'});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err => {
        return next({status : 400, error : err});
    });
});

/*
    headers : {
        Authorization : user JWT accessToken
    }
    query : {
        id : 1
    }
*/
//todo works properly 28.12.17
router.get('/getBusinessCardContactsIds', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    requestValidator.validateGetBusinessCardContacts(req).then(result => {
        let id = req.query.id;
        let userId = req.user.id;

        businessCardsRepo.getCardWithContactsById(id).then(businessCard => {
            let plainCard = businessCard.get({plain : true});
            let arrOfContacts = plainCard.Contacts;
            let contactsToBeReturn = [];

            async.each(arrOfContacts, (currentContact, cb) => {
                contactsToBeReturn.push({
                    userId : currentContact.UserId,
                    contactBusinessCardId : currentContact.CardCard.ContactId
                });
                cb(null);
            }, err => {
                if(err){
                    return next({status : 400, error : err});
                } else {
                    if(businessCard.UserId === userId){
                        res.json({
                            contacts : contactsToBeReturn
                        });
                    } else {
                        return next({status : 401, error : 'Only card owner can obtain list of contacts belongs to given card'});
                    }
                }
            });
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});

/*
    headers : {
        Authorization : user JWT accessToken
    }
    query : {
        id : 1
    }
*/
//todo works properly 28.12.17
router.get('/getBusinessCardById', passport.authenticate('jwt', {session: false}), (req,res,next) => {
    requestValidator.validateGetBusinessCardContacts(req).then(result => {
        let id = req.query.id;
        let userId = req.user.id;

        businessCardsRepo.getByIdWithAssociatedPatternAndCompanyDetails(id).then(businessCard => {
            if(businessCard.UserId === userId){
                res.json({
                    businessCard : businessCard
                });
            } else {

                usersRepo.getUserContacts(businessCard.UserId).then(arrOfContacts => {

                    let doesUserIsInContacts = _.find(arrOfContacts, { UserId : userId});

                    if(doesUserIsInContacts){
                        res.json({
                            businessCard : businessCard
                        });
                    } else {
                        return next({status : 401, error : 'User with id : '+userId+' does not have access to card with id : '+id});
                    }
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});

/*
    headers : {
        Content-Type : application/json
        Authorization : user JWT accessToken
    }
    body : {
        id : 1
    }
*/
//todo works perfectly 28.12.17
router.post('/deleteById', passport.authenticate('jwt', {session: false}), (req,res,next) => {

    requestValidator.validateDeleteById(req).then(result => {
        let id = req.body.id;
        let userId = req.user.id;

        businessCardsRepo.getById(id).then(businessCard => {
            if(businessCard.UserId === userId){
                businessCardsRepo.removeById(id, userId).then(result => {
                    res.json({
                        message : 'Business card with id : '+id+' has been deleted',
                        result : result
                    });
                }).catch(err => {
                    return next({status : 400, error : err});
                });
            } else {
                return next({status : 400, error : 'Business card does not belongs to user with id : '+userId+' therefore it cannot be deleted'});
            }
        }).catch(err => {
            return next({status : 400, error : err});
        });
    }).catch(err =>{
        return next({status : 400, error : err});
    })
});


module.exports = router;