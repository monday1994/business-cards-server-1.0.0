const stripe = require('stripe')(process.env.stripe_secret_key);

exports.handleCharge = function(stripeToken, cashAmount, currency, description){
    return new Promise((resolve, reject) => {
        stripe.charges.create({
            amount: cashAmount,
            currency: currency,
            source: stripeToken,
            description: description,
            metadata: {}
        }, function(err, result){
            if(err){
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
};