const sg = require('sendgrid')(process.env.sendgrid_api_key);
const helper = require('sendgrid').mail;
const mailsContent = require('../configuration/mailsContent');

const sendEmail = function(senderEmail, receiverEmail, subject, content, html){

    let fromEmail = new helper.Email(senderEmail);
    let toEmail = new helper.Email(receiverEmail);
    let transformedContent = new helper.Content('text/plain', content);
    let mail = new helper.Mail(fromEmail, subject, toEmail, transformedContent);

    let request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });
    return new Promise((resolve, reject) => {
        sg.API(request, (err, response) => {
            if (err) {
                reject(err);
            } else {
                console.log("email has been send");
                resolve(true);
            }

        });
    });
};

exports.sendEmail = sendEmail;

exports.sendEmailWithPasswordToNewUser = function(userName, userLastname, generatedPassword, userEmail){

    let tempMail = mailsContent.userRegistrationEmailContent;

    let readyEmail = tempMail.replaceAll('USER_NAME', userName);
    readyEmail = readyEmail.replaceAll('USER_LASTNAME', userLastname);
    readyEmail = readyEmail.replaceAll('GENERATED_PASSWORD', generatedPassword);

    return sendEmail(mailsContent.serverMailAddress, userEmail, mailsContent.userRegistrationEmailSubject, readyEmail, null);
};

exports.sendEmailWithPasswordToNewAdmin = function(adminName, adminLastname, generatedPassword, adminEmail){

    let tempMail = mailsContent.adminRegistrationEmailContent;

    let readyEmail = tempMail.replaceAll('USER_NAME', adminName);
    readyEmail = readyEmail.replaceAll('USER_LASTNAME', adminLastname);
    readyEmail = readyEmail.replaceAll('GENERATED_PASSWORD', generatedPassword);

    return sendEmail(mailsContent.serverMailAddress, adminEmail, mailsContent.adminRegistrationEmailSubject, readyEmail, null);
};

exports.sendEmailWithResetPasswordLink = function(username, link, userEmail){

    let tempMail = mailsContent.forgotPasswordEmailContent;

    let readyEmail = tempMail.replaceAll('USER_NAME', username);
    readyEmail = readyEmail.replaceAll('LINK', link);

    return sendEmail(mailsContent.serverMailAddress, userEmail, mailsContent.forgotPasswordEmailSubject, readyEmail, null);
};

exports.sendEmailAfterResetPassword = function(username, generatedPassword, userEmail){

    let tempMail = mailsContent.afterResetPasswordEmailContent;

    let readyEmail = tempMail.replaceAll('USER_NAME', username);
    readyEmail = readyEmail.replaceAll('NEW_PASSWORD', generatedPassword);

    return sendEmail(mailsContent.serverMailAddress, userEmail, mailsContent.afterResetPasswordEmailSubject, readyEmail, null);
};

exports.sendEmailWithChangedPassword = function(username, changedPassword, userEmail){

    let tempMail = mailsContent.changePasswordEmailContent;

    let readyEmail = tempMail.replaceAll('USER_NAME', username);
    readyEmail = readyEmail.replaceAll('NEW_PASSWORD', changedPassword);

    return sendEmail(mailsContent.serverMailAddress, userEmail, mailsContent.changePasswordEmailSubject, readyEmail, null);
};
