const userRegistrationEmailContent = 'Hi USER_NAME USER_LASTNAME, You have created new account in business cards system. Your password is GENERATED_PASSWORD .';
const userRegistrationEmailSubject = 'Account has been created';
const adminRegistrationEmail = 'Welcome USER_NAME USER_LASTNAME, your admin account has been created in Business cards system. Your password is GENERATED_PASSWORD .';
const adminRegistrationEmailSubject = 'Business cards admin account has been created';
const forgotPasswordEmailContent = 'Hi, USER_NAME, We\'ve received a request to reset your password. If you want to finish the process please click below LINK . New password will be send via email';
const forgotPasswordEmailSubject = 'Business cards - reset password request';

const afterResetPasswordEmailContent = 'Hi, USER_NAME, Your password has been resetted. Your new password is : NEW_PASSWORD .';
const afterResetPasswordEmailSubject = 'Business cards - password has been successfully resetted';

const changePasswordEmailContent = 'Hi, USER_NAME, You have recently changed your password. Your new password is : NEW_PASSWORD .';
const changePasswordEmailSubject = 'Business cards - change password';

const serverMailAddress = 'BUSINESS-CARDS-no-reply@business-cards.com';


module.exports = {
    userRegistrationEmailContent : userRegistrationEmailContent,
    userRegistrationEmailSubject : userRegistrationEmailSubject,
    adminRegistrationEmailContent : adminRegistrationEmail,
    adminRegistrationEmailSubject : adminRegistrationEmailSubject,
    forgotPasswordEmailContent : forgotPasswordEmailContent,
    forgotPasswordEmailSubject : forgotPasswordEmailSubject,
    changePasswordEmailContent : changePasswordEmailContent,
    changePasswordEmailSubject : changePasswordEmailSubject,
    afterResetPasswordEmailContent : afterResetPasswordEmailContent,
    afterResetPasswordEmailSubject : afterResetPasswordEmailSubject,
    serverMailAddress : serverMailAddress
};