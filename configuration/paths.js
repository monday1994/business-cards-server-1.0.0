const path = require('path');

module.exports = {
    pathToCardPatternsDirectory : path.normalize(__dirname+'/../files/cardPatterns'),
    pathToUsersImagesDirectory : path.normalize(__dirname+'/../files/usersImages'),
    linkToProfilePhoto : process.env.server_url+'/users/getProfilePhoto',
    linkToCardPattern : process.env.server_url+'/patterns/getPattern'
};