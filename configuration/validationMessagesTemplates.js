const VP = require('./validationParameters');

const invalidNameMessage = 'name must contains only alphabet characters and be between '+VP.NAME_MIN_LENGTH+' and '+VP.NAME_MAX_LENGTH+' long';
const invalidLastnameMessage = 'lastname must contains only alphabet characters and be between '+VP.LASTNAME_MIN_LENGTH+' and '+VP.LASTNAME_MAX_LENGTH+' long';
const invalidEmailMessage = 'email must match pattern email@domain.com and be between '+VP.EMAIL_MIN_LENGTH+' and '+VP.EMAIL_MAX_LENGTH+' long';
const invalidPasswordMessage = 'password must contains only alpha numeric characters and be between '+VP.PASSWORD_MIN_LENGHT+' and '+VP.PASSWORD_MAX_LENGTH+' long';
const invalidRoleMessage = 'role must be one of '+JSON.stringify(VP.ARRAY_OF_SYSTEM_ROLES);
const invalidRateMessage = 'rate must be between '+VP.RATE_MIN+' and '+VP.RATE_MAX;
const invalidIdMessage = 'ID must be integer and be at least 1';
const invalidPhoneNumber = 'Phone number is not valid. Must match 111-222-333';
const invalidLocation = 'Location is invalid, must contain only alphabet [a-z] letters';
const invalidResetPasswordToken = 'Sent reset password token is not valid';
const invalidProfilePhotoName = 'Sent profile photo name is not valid';
const invalidPriceMessage = 'Price is invalid, must be at minimum 0';
const invalidPatternName = 'Pattern name is invalid';
const invalidKRS = 'KRS must have 10 digits';
const invalidREGON = 'REGON must have 9 or 14 characters';
const invalidNIP = 'NIP must have 10 digitis';
const invalidWebsiteMessage = 'Invalid website, must be in format "https://www.website.domain"';

module.exports = {
    INVALID_NAME_MESSAGE : invalidNameMessage,
    INVALID_LASTNAME_MESSAGE : invalidLastnameMessage,
    INVALID_EMAIL_MESSAGE : invalidEmailMessage,
    INVALID_PASSWORD_MESSAGE : invalidPasswordMessage,
    INVALID_ROLE_MESSAGE : invalidRoleMessage,
    INVALID_RATE_MESSAGE : invalidRateMessage,
    INVALID_ID_MESSAGE : invalidIdMessage,
    INVALID_PHONE_NUMBER : invalidPhoneNumber,
    INVALID_LOCATION : invalidLocation,
    INVALID_RESET_PASSWORD_TOKEN : invalidResetPasswordToken,
    INVALID_PROFILE_PHOTO_NAME : invalidProfilePhotoName,
    INVALID_PRICE_MESSAGE : invalidPriceMessage,
    INVALID_PATTERN_NAME : invalidPatternName,
    INVALID_KRS_MESSAGE : invalidKRS,
    INVALID_NIP_MESSAGE : invalidNIP,
    INVALID_REGON_MESSAGE : invalidREGON,
    INVALID_WEBSITE_MESSAGE : invalidWebsiteMessage
};