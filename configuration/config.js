module.exports = {
    //24 hours
    JWT_EXPIRATION_TIME : 60*60*24,
    profilePictureXdimension : 1000,
    profilePictureYdimension : 1000
};